
from my_test import Test
#https://edabit.com/challenge/YcKh5TokDmm8MZ9Dk
import re

def hidden_anagram(text, phrase):
    text = re.sub("[^a-zA-Z]", "", text).lower()
    phrase = re.sub("[^a-zA-Z]", "", phrase).lower()

    p_length = len(phrase)
    sorted_phrase = ''.join(sorted(phrase))

    for i in range(len(text) - p_length+1):
        part = ''.join(sorted(text[i:i+p_length]))
        #print(part, ' ', sorted_phrase, ' ', text)
        if part == sorted_phrase:
            return text[i:i+p_length]

    return "noutfond"


Test.assert_equals(hidden_anagram("Sir Patrick Moore was a famous moon starer", "Astronomer"), "moonstarer")
Test.assert_equals(hidden_anagram("A building, built to stay free of defects, is uncommon!", "Statue of Liberty"), "builttostayfree")
Test.assert_equals(hidden_anagram('Bright is the moon', 'Bongo mirth'), 'noutfond')
Test.assert_equals(hidden_anagram("Anchor man Bill, a well known TV personality, was confused about becoming president", "Abraham Lincoln"), "anchormanbilla")
#exit(0)
Test.assert_equals(hidden_anagram("There seem to be more and more television ads on the box these days!", "enslave idiots"), "televisionads")
Test.assert_equals(hidden_anagram("The thing orators hate most is a throat infection", "A sore throat"), "oratorshate")
Test.assert_equals(hidden_anagram("I thought I heard a high cornet note of great beuaty", "One Cornetto"), "cornetnoteo")
Test.assert_equals(hidden_anagram('D  e b90it->?$ (c)a r...d,,#~', 'bad credit'), 'debitcard')
Test.assert_equals(hidden_anagram("You won't find any anagram here!", 'aerogramhenna'), 'noutfond')