from my_test import Test

#SUBMIT
#https://edabit.com/challenge/YDgtdP69Mn9pC73xN
def get_surroundings(lst,i,j):
    total = 0
    for k in (-1,0,1):
        if i+k < 0 or i+k >= len(lst):
            continue
        for m in (-1, 0, 1):
            if j + m < 0 or j + m >= len(lst[i]):
                continue
            if lst[i+k][j+m] == '#':
                total += 1

    return str(total)

def num_grid(lst):
    result = []
    for i in range(len(lst)):
        result.insert(i,[])
        for j in range(len(lst[i])):
            if lst[i][j] == "-":
                result[i].insert(j, get_surroundings(lst, i, j))
            else:
                result[i].insert(j, lst[i][j])

    return result


Test.assert_equals(num_grid([
['-', '-', '-', '-', '-'],
['-', '-', '-', '-', '-'],
['-', '-', '#', '-', '-'],
['-', '-', '-', '-', '-'],
['-', '-', '-', '-', '-']
]), [
['0', '0', '0', '0', '0'],
['0', '1', '1', '1', '0'],
['0', '1', '#', '1', '0'],
['0', '1', '1', '1', '0'],
['0', '0', '0', '0', '0']
])

Test.assert_equals(num_grid([
['-', '-', '-', '-', '#'],
['-', '-', '-', '-', '-'],
['-', '-', '#', '-', '-'],
['-', '-', '-', '-', '-'],
['#', '-', '-', '-', '-']
]), [
['0', '0', '0', '1', '#'],
['0', '1', '1', '2', '1'],
['0', '1', '#', '1', '0'],
['1', '2', '1', '1', '0'],
['#', '1', '0', '0', '0']
])

Test.assert_equals(num_grid([
['-', '-', '-', '#', '#'],
['-', '#', '-', '-', '-'],
['-', '-', '#', '-', '-'],
['-', '#', '#', '-', '-'],
['-', '-', '-', '-', '-']
]), [
['1', '1', '2', '#', '#'],
['1', '#', '3', '3', '2'],
['2', '4', '#', '2', '0'],
['1', '#', '#', '2', '0'],
['1', '2', '2', '1', '0']
])