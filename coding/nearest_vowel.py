from my_test import Test
#https://edabit.com/challenge/jWHkKc2pYmgobRL8R
def distance_to_nearest_vowel(txt):
    # build a separate array with -1 so we know there is no distance
    # when meeting a vowel make current position 0, go back until distance
    # is greater or equal than previous calculated distance
    distances = list(reversed(range(len(txt))))
    vowels = ['a','e','i','o','u']
    counter = 0
    found = False
    for i in range(len(txt)):

        if txt[i] in vowels:
            counter = 0
            found = True
            j = i-1
            distances[i] = counter
            # update distances backwards
            while j >= 0 and distances[j] > i-j:
                distances[j] = i - j
                j -= 1

            continue

        counter += 1
        if found:
            distances[i] = counter
    return distances



Test.assert_equals(distance_to_nearest_vowel("aaaaa"), [0, 0, 0, 0, 0])
Test.assert_equals(distance_to_nearest_vowel("bba"), [2, 1, 0])
Test.assert_equals(distance_to_nearest_vowel("abbb"), [0, 1, 2, 3])
Test.assert_equals(distance_to_nearest_vowel("abab"), [0, 1, 0, 1])
Test.assert_equals(distance_to_nearest_vowel("babbb"), [1, 0, 1, 2, 3])
Test.assert_equals(distance_to_nearest_vowel("baaab"), [1, 0, 0, 0, 1])
Test.assert_equals(distance_to_nearest_vowel("abcdabcd"), [0, 1, 2, 1, 0, 1, 2, 3])
Test.assert_equals(distance_to_nearest_vowel("abbaaaaba"), [0, 1, 1, 0, 0, 0, 0, 1, 0])
Test.assert_equals(distance_to_nearest_vowel("treesandflowers"), [2, 1, 0, 0, 1, 0, 1, 2, 2, 1, 0, 1, 0, 1, 2])
Test.assert_equals(distance_to_nearest_vowel("pokerface"), [1, 0, 1, 0, 1, 1, 0, 1, 0])
Test.assert_equals(distance_to_nearest_vowel("beautiful"), [1, 0, 0, 0, 1, 0, 1, 0, 1])
Test.assert_equals(distance_to_nearest_vowel("rythmandblues"), [5, 4, 3, 2, 1, 0, 1, 2, 2, 1, 0, 0, 1])
Test.assert_equals(distance_to_nearest_vowel("shopper"), [2, 1, 0, 1, 1, 0, 1])
Test.assert_equals(distance_to_nearest_vowel("singingintherain"), [1, 0, 1, 1, 0, 1, 1, 0, 1, 2, 1, 0, 1, 0, 0, 1])
Test.assert_equals(distance_to_nearest_vowel("sugarandspice"), [1, 0, 1, 0, 1, 0, 1, 2, 2, 1, 0, 1, 0])
Test.assert_equals(distance_to_nearest_vowel("totally"), [1, 0, 1, 0, 1, 2, 3])