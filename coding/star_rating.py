
from my_test import Test
# https://edabit.com/challenge/2fZETLANpgp6uhTjG

def star_rating(lst):
    total = 0
    votes = 0
    star = 1
    for val in lst:
        total += val * star
        votes += val
        star  +=1
    avg = total/votes
    return "[%.2f] %s" % (round(avg,2),round(avg) * "*")

Test.assert_equals(star_rating([55, 67, 98, 115, 61]), "[3.15] ***")
Test.assert_equals(star_rating([75, 79, 50, 55, 25]), "[2.56] ***")
Test.assert_equals(star_rating([0, 2, 0, 1, 23]), "[4.73] *****")
Test.assert_equals(star_rating([16, 17, 23, 40, 45]), "[3.57] ****")
Test.assert_equals(star_rating([175, 790, 550, 1550, 1245]), "[3.67] ****")
Test.assert_equals(star_rating([0, 0, 0, 0, 5]), "[5.00] *****")
Test.assert_equals(star_rating([6713, 7809, 5350, 5005, 6250]), "[2.88] ***")
Test.assert_equals(star_rating([80, 79, 82, 155, 325]), "[3.79] ****")
