# Author: Emil Dragu<emildragu@gmail.com>
#
# Category: programming exercises
#
# This script splits an amount(of money) into multiple denominations
#
#

def _coins_combinations(amount, denominations, combinations, *current):
    # if amount is 0 we eneded checking this combination

    # DEBUG pprint({'amount': amount, 'current': current_p})

    for coin in denominations:
        new_amount = amount - coin

        #amount still greater, continue with another
        if new_amount < 0:
            continue

        # this variant has come to succesful end, continue
        if new_amount == 0:
            combinations.append(current + (coin,))
            break

        # amount still greater than 0, add this coin and continue
        if new_amount > 0:
            valid_denominations = list(filter(lambda x: x >= coin, denominations))
            _coins_combinations(new_amount, valid_denominations, combinations, *current + (coin,))


def coins_combinations2(money, coins):
    combinations = []
    _coins_combinations(money, sorted(coins), combinations,*[])
    return len(combinations)

def coins_combinations(money, coins):
	A=[0]*(money+1)
	A[0]=1
	for x in coins:
		for i in range(money-x+1):
			A[i+x]+=A[i]
	return A[money]

print(coins_combinations(4, [1,2]), 3)
print(coins_combinations(10, [5,2,3]), 4)
print(coins_combinations(11, [5,7]), 0)
print(coins_combinations(300, [5,10,20,50,100,200,500]), 1022)
print(coins_combinations(300, [500,5,50,100,20,200,10]), 1022)
print(coins_combinations(301, [5,10,20,50,100,200,500]), 0)
print(coins_combinations(199, [3,5,9,15]), 760)
print(coins_combinations(98, [3,14,8]), 19)
print(coins_combinations(419, [2,5,10,20,50]), 18515)
