
from my_test import Test
# https://edabit.com/challenge/SHdu4GwBQehhDm4xT

def freed_prisoners(prison):
    freed = 0
    locked = 0
    unlocked = 1

    if prison[0] == locked:
        return 0

    for i in range(len(prison)):
        if prison[i] == unlocked:
            freed += 1
            # switch the locks
            tmp = locked
            locked = unlocked
            unlocked = tmp

    return freed


Test.assert_equals(freed_prisoners([1, 1, 0, 0, 0, 1, 0]), 4)
Test.assert_equals(freed_prisoners([1, 0, 0, 0, 0, 0, 0]), 2)
Test.assert_equals(freed_prisoners([1, 1, 1, 0, 0, 0]), 2)
Test.assert_equals(freed_prisoners([1, 0, 1, 0, 1, 0]), 6)
Test.assert_equals(freed_prisoners([1, 1, 1]), 1, 'once first prisoner freed, all cells become locked')
Test.assert_equals(freed_prisoners([0, 0, 0]), 0, 'first cell locked, so cannot switch')
Test.assert_equals(freed_prisoners([0, 1, 1, 1]), 0, 'first cell locked, cannot switch')