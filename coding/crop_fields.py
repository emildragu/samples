
from my_test import Test
#https://edabit.com/challenge/FmowTJecDKQMRqsHS

def crop_hydrated(field):
    # search for a c cell that does not gave a w around it
    for i in range(len(field)):
        for j in range(len(field[i])):
            # TODO we can skip at least one j if cell is watter
            if field[i][j] == 'w':
                continue

            # we need to check
            if i-1 >= 0:
                if j-1 >= 0 and field[i-1][j-1] == 'w':
                    continue

                if field[i - 1][j] == 'w':
                    continue

                if j+1 < len(field[i]) and field[i-1][j+1] == 'w':
                    continue

            if j-1 >= 0 and field[i][j-1] == 'w':
                continue

            if j+1 < len(field[i]) and field[i][j+1] == 'w':
                continue

            if i+1 <  len(field):
                if j-1 >= 0 and field[i+1][j-1] == 'w':
                    continue

                if field[i+1][j] == 'w':
                    continue

                if j+1 < len(field[i]) and field[i+1][j+1] == 'w':
                    continue

            return False

    return True




Test.assert_equals(crop_hydrated([
  [ "w", "w" ],
  [ "w", "c" ],
  [ "c", "c" ],
  [ "c", "w" ],
  [ "c", "w" ]
]), True)

Test.assert_equals(crop_hydrated([
  [ "c", "w", "w", "w", "c" ],
  [ "w", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c" ],
  [ "w", "c", "c", "w", "c" ]
]), True)

Test.assert_equals(crop_hydrated([
  [ "c", "w" ]
]), True)

Test.assert_equals(crop_hydrated([
  [ "w", "c", "c", "c", "c" ],
  [ "c", "c", "c", "w", "c" ]
]), True)

Test.assert_equals(crop_hydrated([
  [ "c", "c", "c" ],
  [ "w", "w", "c" ],
  [ "c", "c", "c" ],
  [ "w", "w", "c" ],
  [ "c", "c", "c" ],
  [ "c", "c", "c" ],
  [ "c", "w", "c" ]
]), True)

Test.assert_equals(crop_hydrated([
  [ "c", "c", "c" ],
  [ "w", "w", "c" ]
]), True)

Test.assert_equals(crop_hydrated([
  [ "c", "w", "w", "c", "c", "w", "c" ]
]), True)

Test.assert_equals(crop_hydrated([
  [ "c", "w", "c", "c", "w", "w" ],
  [ "c", "c", "w", "c", "c", "c" ],
  [ "w", "c", "c", "c", "c", "w" ],
  [ "c", "w", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "w", "w" ]
]), True)

Test.assert_equals(crop_hydrated([
  [ "c" ],
  [ "w" ],
  [ "c" ],
  [ "c" ],
  [ "w" ],
  [ "c" ]
]), True)

Test.assert_equals(crop_hydrated([
  [ "c", "c", "w", "w", "c", "c", "c" ],
  [ "c", "w", "c", "w", "w", "c", "w" ],
  [ "w", "w", "c", "w", "c", "c", "c" ]
]), True)


Test.assert_equals(crop_hydrated([
  [ "c", "c", "w", "c", "c", "w", "c", "w" ]
]), False)

Test.assert_equals(crop_hydrated([
  [ "c", "c", "c", "c", "c", "w", "c" ],
  [ "c", "w", "c", "c", "w", "c", "w" ],
  [ "c", "c", "c", "w", "c", "w", "c" ],
  [ "w", "w", "c", "c", "c", "c", "c" ],
  [ "c", "c", "w", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "w", "c", "c" ],
  [ "w", "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c", "c" ],
  [ "w", "c", "c", "c", "c", "c", "w" ]
]), False)

Test.assert_equals(crop_hydrated([
  [ "c", "w", "c", "c" ],
  [ "w", "c", "c", "c" ],
  [ "c", "c", "c", "c" ],
  [ "w", "c", "c", "c" ],
  [ "w", "w", "c", "c" ],
  [ "c", "w", "c", "c" ],
  [ "c", "c", "w", "c" ],
  [ "c", "c", "w", "w" ],
  [ "c", "c", "c", "w" ]
]), False)

Test.assert_equals(crop_hydrated([
  [ "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c" ],
  [ "w", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "w" ],
  [ "c", "c", "c", "c", "w", "c" ],
  [ "c", "w", "w", "c", "c", "c" ]
]), False)

Test.assert_equals(crop_hydrated([
  [ "c", "c", "c", "c", "c", "w", "c" ],
  [ "w", "c", "c", "c", "c", "c", "w" ],
  [ "c", "c", "c", "c", "c", "c", "c" ],
  [ "c", "w", "w", "c", "c", "w", "w" ],
  [ "c", "c", "w", "c", "w", "c", "c" ],
  [ "w", "c", "c", "c", "w", "c", "c" ],
  [ "c", "c", "c", "c", "w", "c", "c" ],
  [ "w", "c", "c", "c", "c", "c", "c" ]
]), False)

Test.assert_equals(crop_hydrated([
  [ "c", "w", "c", "c", "w", "c", "c", "c", "w" ],
  [ "c", "c", "c", "c", "c", "c", "c", "c", "c" ],
  [ "w", "c", "c", "c", "w", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "w", "w", "w", "c" ],
  [ "w", "c", "c", "w", "w", "c", "c", "c", "w" ],
  [ "c", "c", "c", "c", "w", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c", "c", "c", "c" ]
]), False)

Test.assert_equals(crop_hydrated([
  [ "c", "c", "w", "c", "w" ],
  [ "c", "c", "c", "c", "c" ],
  [ "w", "c", "w", "c", "c" ],
  [ "c", "w", "w", "c", "c" ],
  [ "c", "c", "c", "c", "w" ],
  [ "c", "c", "c", "w", "c" ]
]), False)

Test.assert_equals(crop_hydrated([
  [ "c", "w", "c", "c", "c", "w", "w", "c" ],
  [ "c", "c", "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c", "c", "c" ],
  [ "w", "c", "c", "c", "c", "c", "w", "c" ]
]), False)

Test.assert_equals(crop_hydrated([
  [ "w", "w", "c", "c", "w" ],
  [ "c", "c", "c", "c", "c" ],
  [ "c", "c", "w", "c", "c" ],
  [ "w", "c", "c", "w", "w" ],
  [ "c", "c", "w", "c", "c" ],
  [ "c", "c", "w", "c", "c" ],
  [ "c", "c", "c", "w", "c" ]
]), False)

Test.assert_equals(crop_hydrated([
  [ "c", "c", "w", "c", "c", "w" ],
  [ "c", "w", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "w", "c", "c" ],
  [ "c", "c", "c", "c", "w", "c" ],
  [ "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c" ],
  [ "c", "c", "c", "c", "c", "c" ]
]), False)

# By Harith Shah