from my_test import Test
# https://edabit.com/challenge/2C3gtb4treAFyWJMg
import re

def encode_letter(letter):
    if letter == ' ':
        return letter
    ascii = ord(letter) - 97

    if ascii >= 9:
        ascii = ascii - 1

    x = str(int((ascii) / 5) + 1)
    y = str(((ascii) % 5) + 1)

    return x+y

def encode(text):
    text = re.sub("[^a-zA-Z ]", "", text).lower()
    encoded = ''

    for letter in text:
        encoded = encoded + encode_letter(letter)

    return encoded

def decode_letter(letter):
    if letter == ' ':
        return ' '
    val = 5 * (int(letter[0])-1) + int(letter[1])

    if val <= 9:
        val = val -1

    return chr(97 + val)

def decode(text):

    decoded = ''
    regex = re.compile("(?:(\d{2}|\s))")

    m = re.findall(regex, text)

    for letter in m:
        decoded = decoded + decode_letter(letter)

    return decoded

def polybius(text):
    regex = re.compile('[a-zA-Z]')
    if regex.match(text):
        return encode(text)
    else:
        return decode(text)

Test.assert_equals(polybius('4323343531242144243322 2443 11 51241344243231154343 1342243215 31242515 3545331323243322 43343215343315 2433 442315 14114225'), "shoplifting is a victimless crime like punching someone in the dark")
Test.assert_equals(polybius('Hi'), '2324')
Test.assert_equals(polybius("Just because I don't care doesn't mean that I don't understand"), '24454344 12151311454315 24 14343344 13114215 143415433344 32151133 44231144 24 14343344 45331415424344113314', "Disregard punctuation, but keep spaces")
Test.assert_equals(polybius('24454344 12151311454315 24 14343344 13114215 143415433344 32151133 44231144 24 14343344 45331415424344113314'), 'iust because i dont care doesnt mean that i dont understand')
Test.assert_equals(polybius('543445 14343344 522433 21422415331443 52244423 4311311114'), 'you dont win friends with salad')
Test.assert_equals(polybius('The lesson is: never try'), '442315 311543433433 2443 3315511542 444254')

# Credit: Simpsons