
from my_test import Test

#https://edabit.com/challenge/phMpXM9nu52bCjguS

def change_enough2(change, amount_due):
    values = [0.25, 0.10, 0.05, 0.01]
    i = 0
    while i < len(values):
        count = change[i]
        amount = values[i]
        i += 1
        while count > 0:
            if amount_due - amount == 0:
                return True
            # if giving this coin is more, try the next coin
            if amount_due - amount < 0:
                break

            amount_due -= amount
            amount_due = round(amount_due,2)
            count -= 1

    return False


Test.assert_equals(change_enough([2, 100, 0, 0], 14.11), False)
Test.assert_equals(change_enough([0, 0, 20, 5], 0.75), True)
#exit(0)
Test.assert_equals(change_enough([30, 40, 20, 5], 12.55), True)
Test.assert_equals(change_enough([10, 0, 0, 50], 13.85), False)
Test.assert_equals(change_enough([1, 0, 5, 219], 19.99), False)
Test.assert_equals(change_enough([1, 0, 2555, 219], 127.75), True)
Test.assert_equals(change_enough([1, 335, 0, 219], 35.21), True)