class Test:
    @classmethod
    def assert_equals(cls, result, expected,msg=""):
        if result == expected:
            print('Test OK [%s]' % msg)
            return True
        else:
            print('Test FAILED [Expected %s got %s] [%s]' % (expected, result, msg))
            return False
