
from my_test import Test
#https://edabit.com/challenge/A8gEGRXqMwRWQJvBf

def tic_tac_toe(board):
    length = len(board)

    for i in range(length):
        x = board[i][0]
        y = board[0][i]

        for j in range(1,length):
            if not x and not y:
                break
            if x != board[i][j]:
                x = False
            if y != board[j][i]:
                y = False
        if y and y != 'E':
            return y
        if x and x != 'E':
            return x

    x = board[0][0]
    y = board[0][length-1]

    for i in range(1,length):

        if not x and not y:
            break

        if x != board[i][i]:
            x = False

        if y != board[i][length-1-i]:
            y = False

    if y and y != 'E':
        return y
    if x and x != 'E':
        return x

    return 'Draw'

Test.assert_equals(tic_tac_toe([
    ["X", "O", "X"],
    ["O", "X", "O"],
    ["O", "X", "X"]
]), "X")

Test.assert_equals(tic_tac_toe([
    ["O", "O", "O"],
    ["O", "X", "X"],
    ["E", "X", "X"]
]), "O")

Test.assert_equals(tic_tac_toe([
    ["X", "X", "O"],
    ["O", "O", "X"],
    ["X", "X", "O"]
]), "Draw")

Test.assert_equals(tic_tac_toe([
    ["X", "O", "O"],
    ["X", "O", "O"],
    ["X", "X", "X"]
]), "X")

Test.assert_equals(tic_tac_toe([
    ["X", "X", "O"],
    ["O", "O", "X"],
    ["X", "X", "O"]
]), "Draw")

Test.assert_equals(tic_tac_toe([
    ["X", "O", "X"],
    ["O", "X", "O"],
    ["E", "E", "X"]
]), "X")

Test.assert_equals(tic_tac_toe([
    ["X", "O", "E"],
    ["X", "O", "E"],
    ["E", "O", "X"]
]), "O")
Test.assert_equals(tic_tac_toe([
    ["X", "E", "O"],
    ["X", "O", "E"],
    ["O", "O", "X"]
]), "O")

Test.assert_equals(tic_tac_toe([
    ["O", "O", "E","X"],
    ["X", "O", "X","X"],
    ["E", "X", "X","X"],
    ["X", "O", "X","O"]
]), "X")
# By Harith Shah